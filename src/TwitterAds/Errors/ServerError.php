<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Errors;

use Exception;
use Hborras\TwitterAdsSDK\TwitterAdsException;

/**
 * Class ServerError
 * @package Hborras\TwitterAdsSDK\TwitterAds\Errors
 */
class ServerError extends TwitterAdsException
{

}
