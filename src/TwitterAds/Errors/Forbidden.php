<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Errors;

use Exception;
use Hborras\TwitterAdsSDK\TwitterAdsException;

/**
 * Class Forbidden
 * @package Hborras\TwitterAdsSDK\TwitterAds\Errors
 */
class Forbidden extends TwitterAdsException
{

}
