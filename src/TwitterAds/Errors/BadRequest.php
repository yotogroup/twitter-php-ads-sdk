<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Errors;

use Exception;
use Hborras\TwitterAdsSDK\TwitterAdsException;

/**
 * Class BadRequest
 * @package Hborras\TwitterAdsSDK\TwitterAds\Errors
 */
class BadRequest extends TwitterAdsException
{

}
