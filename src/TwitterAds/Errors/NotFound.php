<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Errors;

use Exception;
use Hborras\TwitterAdsSDK\TwitterAdsException;

/**
 * Class NotFound
 * @package Hborras\TwitterAdsSDK\TwitterAds\Errors
 */
class NotFound extends TwitterAdsException
{

}
