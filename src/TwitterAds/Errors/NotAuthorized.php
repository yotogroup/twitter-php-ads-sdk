<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Errors;

use Exception;
use Hborras\TwitterAdsSDK\TwitterAdsException;

/**
 * Class NotAuthorized
 * @package Hborras\TwitterAdsSDK\TwitterAds\Errors
 */
class NotAuthorized extends TwitterAdsException
{

}
