<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Fields;

/**
 * Class MediaFields
 * @package Hborras\TwitterAdsSDK\TwitterAds\Fields
 */
class MediaFields
{
    const MEDIA_KEY             = 'media_key';
    const CREATED_AT            = 'created_at';
    const UPDATED_AT            = 'updated_at';
    const DELETED               = 'deleted';
    const MEDIA_TYPE            = 'media_type';

    const NAME                  = 'name';
    const DESCRIPTION           = 'description';
    const FILE_NAME             = 'file_name';
    const POSTER_MEDIA_KEY      = 'poster_media_key';
    const TITLE                 = 'title';

    const TWEETED               = 'tweeted';
    const DURATION              = 'duration';
    const MEDIA_URL             = 'media_url';
    const MEDIA_CATEGORY        = 'media_category';
    const POSTER_MEDIA_URL      = 'poster_media_url';
    const MEDIA_STATUS          = 'media_status';
    const ASPECT_RATIO          = 'aspect_ratio';

    const ACCOUNT_ID            = 'account_id';
    const COUNT                 = 'count';
    const Q                     = 'q';
    const WITH_DELETED          = 'with_deleted';
}
