<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Fields;

/**
 * Class WebsiteTagFields
 * @package Hborras\TwitterAdsSDK\TwitterAds\Fields
 */
class WebsiteTagFields extends Fields
{
    const ID               = 'id';
    const ALLOW_1P_COOKIE  = 'allow_1p_cookie';
    const DELETED          = 'deleted';
    const EMBED_CODE       = 'embed_code';
    const TAG_TYPE         = 'tag_type';
}
