<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Fields;

/**
 * Class CardFields
 * @package Hborras\TwitterAdsSDK\TwitterAds\Fields
 */
class CardTypes
{
    const IMAGE_APP                                     = 'IMAGE_APP';
    const IMAGE_CAROUSEL_APP                            = 'IMAGE_CAROUSEL_APP';
    const IMAGE_CAROUSEL_WEBSITE                        = 'IMAGE_CAROUSEL_WEBSITE';
    const IMAGE_MULTI_DEST_CAROUSEL_APP                 = 'IMAGE_MULTI_DEST_CAROUSEL_APP';
    const IMAGE_MULTI_DEST_CAROUSEL_WEBSITE             = 'IMAGE_MULTI_DEST_CAROUSEL_WEBSITE';
    const IMAGE_WEBSITE                                 = 'IMAGE_WEBSITE';
    const MIXED_MEDIA_MULTI_DEST_CAROUSEL_APP           = 'MIXED_MEDIA_MULTI_DEST_CAROUSEL_APP';
    const MIXED_MEDIA_MULTI_DEST_CAROUSEL_WEBSITE       = 'MIXED_MEDIA_MULTI_DEST_CAROUSEL_WEBSITE';
    const MIXED_MEDIA_SINGLE_DEST_CAROUSEL_APP          = 'MIXED_MEDIA_SINGLE_DEST_CAROUSEL_APP';
    const MIXED_MEDIA_SINGLE_DEST_CAROUSEL_WEBSITE      = 'MIXED_MEDIA_SINGLE_DEST_CAROUSEL_WEBSITE';
    const VIDEO_APP                                     = 'VIDEO_APP';
    const VIDEO_CAROUSEL_APP                            = 'VIDEO_CAROUSEL_APP';
    const VIDEO_CAROUSEL_WEBSITE                        = 'VIDEO_CAROUSEL_WEBSITE';
    const VIDEO_MULTI_DEST_CAROUSEL_APP                 = 'VIDEO_MULTI_DEST_CAROUSEL_APP';
    const VIDEO_MULTI_DEST_CAROUSEL_WEBSITE             = 'VIDEO_MULTI_DEST_CAROUSEL_WEBSITE';
    const VIDEO_WEBSITE                                 = 'VIDEO_WEBSITE';
}
