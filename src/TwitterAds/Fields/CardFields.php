<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Fields;

/**
 * Class CardFields
 * @package Hborras\TwitterAdsSDK\TwitterAds\Fields
 */
class CardFields
{
    const NAME               = 'name';
    const CREATED_AT         = 'created_at';
    const UPDATED_AT         = 'updated_at';
    const DELETED            = 'deleted';
    const COMPONENTS         = 'components';
    const SLIDES             = 'slides';
    const WITH_TOTAL_COUNT   = 'with_total_count';
    const CARD_TYPE          = 'card_type';
    const CARD_URIS          = 'card_uris';
    const CARD_URI           = 'card_uri';
    const TYPE               = 'type';
}
