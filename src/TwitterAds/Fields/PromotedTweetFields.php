<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Fields;

/**
 * Class PromotedTweetFields
 * @package Hborras\TwitterAdsSDK\TwitterAds\Fields
 */
class PromotedTweetFields extends Fields
{
    const ID                 = 'id';
    const CREATED_AT         = 'created_at';
    const UPDATED_AT         = 'updated_at';
    const DELETED            = 'deleted';
    const APPROVAL_STATUS    = 'approval_status';
    const TWEET_ID           = 'tweet_id';
    const TWEET_IDS          = 'tweet_ids';
    const SCHEDULED_TWEET_ID = 'scheduled_tweet_id';
    const ENTITY_STATUS      = 'entity_status';
    const PROMOTED_TWEET_IDS = 'promoted_tweet_ids';
    const LINE_ITEM_IDS      = 'line_item_ids';
    const LINE_ITEM_ID       = 'line_item_id';
}
