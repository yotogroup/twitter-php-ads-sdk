<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Fields;

/**
 * Class TweetFields
 * @package Hborras\TwitterAdsSDK\TwitterAds\Fields
 */
class TweetFields
{
    const MEDIA_IDS                     = 'media_ids';
    const ID                            = 'id';
    const NAME                          = 'name';
    const TEXT                          = 'text';
    const FULL_TEXT                     = 'full_text';
    const CARD_URI                      = 'card_uri';
    const CREATED_AT                    = 'created_at';
    const UPDATED_AT                    = 'updated_at';
    const DELETED                       = 'deleted';

    const TWEET_IDS                     = 'tweet_ids';
    const TWEET_TYPE                    = 'tweet_type';
    const LANG                          = 'lang';
}
