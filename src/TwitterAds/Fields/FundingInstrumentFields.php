<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Fields;

/**
 * Class FundingInstrumentFields
 * @package Hborras\TwitterAdsSDK\TwitterAds\Fields
 */
class FundingInstrumentFields
{
    const ID                        = 'id';
    const NAME                      = 'name';
    const CANCELLED                 = 'cancelled';
    const CREDIT_LIMIT_LOCAL_MICRO  = 'credit_limit_local_micro';
    const CURRENCY                  = 'currency';
    const DESCRIPTION               = 'description';
    const FUNDED_AMOUNT_LOCAL_MICRO = 'funded_amount_local_micro';
    const TYPE                      = 'type';
    const CREATED_AT                = 'created_at';
    const UPDATED_AT                = 'updated_at';
    const DELETED                   = 'deleted';
    const WITH_TOTAL_COUNT          = 'with_total_count';

    const IO_HEADER                     = 'io_header';
    const ABLE_TO_FUND                  = 'able_to_fund';
    const REASONS_NOT_ABLE_TO_FUND      = 'reasons_not_able_to_fund';
    const CREDIT_REMAINING_LOCAL_MICRO  = 'credit_remaining_local_micro';
    const ENTITY_STATUS                 = 'entity_status';
    const START_TIME                    = 'start_time';
    const END_TIME                      = 'end_time';
}
