<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Fields;

/**
 * Class WebEventTagFields
 * @package Hborras\TwitterAdsSDK\TwitterAds\Fields
 */
class WebEventTagFields extends Fields
{
    const ID                    = 'id';
    const NAME                  = 'name';
    const WEBSITE_TAG_ID        = 'website_tag_id';
    const VIEW_THROUGH_WINDOW   = 'view_through_window';
    const CLICK_WINDOW          = 'click_window';
    const EMBED_CODE            = 'embed_code';
    const RETARGETING_ENABLED   = 'retargeting_enabled';
    const AUTO_CREATED          = 'auto_created';
    const STATUS                = 'status';
    const TYPE                  = 'type';
    const CREATED_AT            = 'created_at';
    const UPDATED_AT            = 'updated_at';
    const DELETED               = 'deleted';

    const ACCOUNT_ID            = 'account_id';
    const COUNT                 = 'count';
    const SORT_BY               = 'sort_by';
    const WEB_EVENT_TAG_IDS     = 'web_event_tag_ids';
    const WITH_TOTAL_COUNT      = 'with_total_count';
    const WITH_DELETED          = 'with_deleted';
}
