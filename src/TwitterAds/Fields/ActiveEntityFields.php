<?php

namespace Hborras\TwitterAdsSDK\TwitterAds\Fields;

/**
 * Class TweetFields
 * @package Hborras\TwitterAdsSDK\TwitterAds\Fields
 */
class ActiveEntityFields
{
    const ENTITY_ID                     = 'entity_id';
    const ACTIVITY_START_TIME           = 'activity_start_time';
    const ACTIVITY_END_TIME             = 'activity_end_time';
    const PLACEMENTS                    = 'placements';

    const ENTITY                        = 'entity';
    const START_TIME                    = 'start_time';
    const END_TIME                      = 'end_time';
    const CAMPAIGN_IDS                  = 'campaign_ids';
    const LINE_ITEM_IDS                 = 'line_item_ids';
}
